//
// Created by forkjoin on 04.03.2022.
//
#include "include/util.h"
#include <stdlib.h>

struct image create_image(uint32_t width, uint32_t height) {
    return (struct image) {.width = width, .height = height, .data = malloc(sizeof(struct pixel) * width * height)};
}

void destroy_image(struct image *image) {
    if (NULL == image) return;
    free(image->data);
}
