//
// Created by forkjoin on 04->03->2022->
//

#include "include/transform.h"
#include <stdlib.h>


struct image rotate(struct image const source) {
    struct image dest = create_image(source.height, source.width);

    if (NULL == dest.data) return dest;

    for (int32_t i = 0; i < source.height; i++) {
        for (int32_t j = 0; j < source.width; j++) {
            const struct pixel px = source.data[i * source.width + j];
            int32_t x = j;
            int32_t y = source.height - 1 - i;
            dest.data[x * dest.width + y] = px;
        }
    }
    return dest;
}

