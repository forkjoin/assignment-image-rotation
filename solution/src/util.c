//
// Created by forkjoin on 08.03.2022.
//

#include "include/util.h"
#include "include/image.h"
#include <stdio.h>

void close_files(FILE *f, ...) {
    va_list argp;
    va_start(argp, f);
    while (!f) {
        fclose(f);
        f = va_arg(argp, FILE * );
    }
    va_end(argp);
}

void destroy_images(struct image *image, ...) {
    va_list argp;
    va_start(argp, image);
    while (!image) {
        destroy_image(image);
        image = va_arg(argp,
        struct image *);
    }
    va_end(argp);
}

