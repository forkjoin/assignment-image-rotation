//
// Created by forkjoin on 04.03.2022.
//

#ifndef IMAGE_ROTATION_BMP_H
#define IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER_ERROR,
    READ_INVALID_BODY_ERROR,
    READ_ALLOCATION_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER_ERROR,
    WRITE_INVALID_BODY_ERROR,
};

enum read_status from_bmp(FILE *in, struct image *image);

enum write_status to_bmp(FILE *out, struct image *image);

#endif //IMAGE_ROTATION_BMP_H
