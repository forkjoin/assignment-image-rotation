//
// Created by forkjoin on 08.03.2022.
//

#ifndef IMAGE_ROTATION_UTIL_H
#define IMAGE_ROTATION_UTIL_H

#endif //IMAGE_ROTATION_UTIL_H

#include "image.h"
#include <stdio.h>

enum file_open_status {
    OPENED_OK,
    OPEN_ERROR
};


void close_file_with_stream_message(FILE *src, FILE *dest, const char *message);

void close_files(FILE *f, ...);

void destroy_images(struct image *image, ...);
