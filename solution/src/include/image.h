//
// Created by forkjoin on 04.03.2022.
//

#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(uint32_t width, uint32_t height);

void destroy_image(struct image *image);

#endif //IMAGE_ROTATION_IMAGE_H
