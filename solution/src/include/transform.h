//
// Created by forkjoin on 04.03.2022.
//

#ifndef IMAGE_ROTATION_TRANSFORM_H
#define IMAGE_ROTATION_TRANSFORM_H

#include "image.h"

struct image rotate(struct image const source);

#endif //IMAGE_ROTATION_TRANSFORM_H
