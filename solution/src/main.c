#include "include/bmp.h"
#include "include/image.h"
#include "include/transform.h"
#include "include/util.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    FILE *in = NULL;
    FILE *out = NULL;
    if (3 != argc) {
        printf("Invalid amount of arguments! Usage: ./image-transformer <source-image><transformed-image>\n");
        return 1;
    }

    if (NULL == (in = fopen(argv[1], "rb"))) {
        fclose(in);
        fprintf(stderr, "Can't read source file!\n");
        return 1;
    }

    if (NULL == (out = fopen(argv[2], "wb"))) {
        close_files(in, out);
        fprintf(stderr, "Can't read destination file!\n");
        return 1;
    }

    struct image image = {0};

    if (READ_OK != from_bmp(in, &image)) {
        close_files(in, &out);
        destroy_image(&image);
        fprintf(stderr, "Can't read bmp image!\n");
        return 1;
    }

    struct image rotated = rotate(image);

    if (NULL == rotated.data) {
        destroy_image(&image);
        destroy_image(&rotated);
        close_files(in, out);
        fprintf(stderr, "Cannot rotate image\n");
        return 1;
    }

    if (WRITE_OK != to_bmp(out, &rotated)) {
        destroy_image(&image);
        destroy_image(&rotated);
        close_files(in, out);
        fprintf(stderr, "Can't' write rotated bmp image!\n");
        return 1;
    }

    printf("Image was rotated successfully!\n");

    destroy_image(&image);
    destroy_image(&rotated);
    close_files(in, out);
    return 0;
}
