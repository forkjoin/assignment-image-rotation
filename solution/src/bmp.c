//
// Created by forkjoin on 04.03.2022.
//

#include "include/image.h"
#include "include/bmp.h"
#include <inttypes.h>
#include <stdlib.h>

#define MAGIC_BMP_TYPE 19778
#define MAGIC_OFF_BITS 54
#define MAGIC_BIT_COUNT 24

#define ZERO 0

static uint32_t bytes_padding(const uint32_t width) {
    uint32_t padding = (width * sizeof(struct pixel)) % 4;
    return 0 == padding % 4 ? padding : 4 - padding;
}

static struct bmp_header create_bmp_header(const struct image *const image) {
    return (struct bmp_header) {
            .bfType = MAGIC_BMP_TYPE,
            .bOffBits = MAGIC_OFF_BITS,
            .biWidth = image->width,
            .biHeight = image->height,
            .biBitCount = MAGIC_BIT_COUNT
    };
}

static enum read_status read_bmp_header(struct bmp_header *const bmp_header, FILE *in) {
    if (!fread(bmp_header, sizeof(struct bmp_header), 1, in))
        return READ_INVALID_HEADER_ERROR;
    return READ_OK;
}

static enum read_status read_bmp_body(FILE *in, struct image *const image) {
    const size_t padding = bytes_padding(image->width);
    for (size_t i = 0; i < image->height; i++) {
        size_t r_width = fread(&image->data[image->width * i], sizeof(struct pixel), image->width, in);
        if (r_width != image->width)
            return READ_INVALID_BODY_ERROR;

        if (fseek(in, padding, SEEK_CUR))
            return READ_INVALID_BODY_ERROR;
    }
    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *const image) {
    struct bmp_header bmp_header = {0};
    enum read_status rs_header = read_bmp_header(&bmp_header, in);
    if (READ_OK != rs_header)
        return rs_header;

    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);
    if (!image->data)
        return READ_ALLOCATION_ERROR;

    enum read_status rs_body = read_bmp_body(in, image);

    if (READ_OK != rs_body)
        return rs_body;

    return READ_OK;
}

static enum write_status write_bmp_header(const struct bmp_header *const bmp_header, FILE *out) {
    if (!fwrite(bmp_header, sizeof(struct bmp_header), 1, out))
        return WRITE_INVALID_HEADER_ERROR;

    if (fseek(out, bmp_header->bOffBits, SEEK_SET))
        return WRITE_INVALID_HEADER_ERROR;
    return WRITE_OK;
}

static enum write_status write_bmp_body(FILE *out, const struct image *const image) {
    const size_t padding = bytes_padding(image->width);

    if (NULL != image->data) {
        for (size_t i = 0; i < image->height; i++) {
            fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out);
            for (size_t j = 0; j < padding; j++) {
                if (putc(ZERO, out))
                    return WRITE_INVALID_BODY_ERROR;
            }
        }
    } else return WRITE_INVALID_BODY_ERROR;

    return WRITE_OK;
}

enum write_status to_bmp(FILE *out, struct image *const image) {
    struct bmp_header bmp_header = create_bmp_header(image);

    enum write_status ws_header = write_bmp_header(&bmp_header, out);

    if (WRITE_OK != ws_header)
        return ws_header;

    enum write_status ws_body = write_bmp_body(out, image);

    if (WRITE_OK != ws_body)
        return ws_body;

    return WRITE_OK;
}
